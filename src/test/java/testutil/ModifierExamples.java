package testutil;

public abstract class ModifierExamples {

    @SuppressWarnings("unused")
    private int privateField;

    int packageField;

    protected int protectedField;

    public int publicField;

    transient int transientField;

    volatile int volatileField;

    final int finalField = 0;

    static int staticField;

    public abstract void abstractMethod();

    final class FinalClass {
        // 0
    }

    interface InterfaceClass {
        // 1
    }

    class PackageClass {
        // 2
    }

    @SuppressWarnings("unused")
    private class PrivateClass {
        // 3
    }

    protected class ProtectedClass {
        // 4
    }

    static class StaticClass {
        // 5
    }

}
