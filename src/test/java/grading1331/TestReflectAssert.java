package grading1331;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import testutil.ModifierExamples;

public class TestReflectAssert {

    @Test
    public void testAssertPrivateMember1() {
        ReflectAssert.assertPrivate(ReflectHelper.getDeclaredField(ModifierExamples.class, "privateField"));
    }

    @Test
    public void testAssertPrivateClass1() throws ClassNotFoundException {
        ReflectAssert.assertPrivate(Class.forName("testutil.ModifierExamples$PrivateClass"));
    }

    @Test
    public void testAssertPrivateMember2() {
        ReflectAssert.assertPrivate("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "privateField"));
    }

    @Test
    public void testAssertPrivateClass2() throws ClassNotFoundException {
        ReflectAssert.assertPrivate("a", Class.forName("testutil.ModifierExamples$PrivateClass"));
    }

    @Test
    public void testAssertPrivateMemberFail1() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertPrivate(ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertPrivateClassFail1() throws ClassNotFoundException {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertPrivate(Class.forName("testutil.ModifierExamples$ProtectedClass"))
        );
    }

    @Test
    public void testAssertPrivateMemberFail2() {
        assertThrows(
            AssertionError.class,
            () -> ReflectAssert.assertPrivate("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertPrivateClassFail2() throws ClassNotFoundException {
        assertThrows(
            AssertionError.class,
            () -> ReflectAssert.assertPrivate("a", Class.forName("testutil.ModifierExamples$ProtectedClass"))
        );
    }

    @Test
    public void testAssertProtectedMember1() {
        ReflectAssert.assertProtected(ReflectHelper.getDeclaredField(ModifierExamples.class, "protectedField"));
    }

    @Test
    public void testAssertProtectedClass1() throws ClassNotFoundException {
        ReflectAssert.assertProtected(Class.forName("testutil.ModifierExamples$ProtectedClass"));
    }

    @Test
    public void testAssertProtectedMember2() {
        ReflectAssert.assertProtected("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "protectedField"));
    }

    @Test
    public void testAssertProtectedClass2() throws ClassNotFoundException {
        ReflectAssert.assertProtected("a", Class.forName("testutil.ModifierExamples$ProtectedClass"));
    }

    @Test
    public void testAssertProtectedMemberFail1() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertProtected(ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertProtectedClassFail1() throws ClassNotFoundException {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertProtected(Class.forName("testutil.ModifierExamples$PrivateClass"))
        );
    }

    @Test
    public void testAssertProtectedMemberFail2() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertProtected("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertProtectedClassFail2() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertProtected("a", Class.forName("testutil.ModifierExamples$PrivateClass"))
        );
    }

    //

    @Test
    public void testAssertPublicMember1() {
        ReflectAssert.assertPublic(ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"));
    }

    @Test
    public void testAssertPublicClass1() {
        ReflectAssert.assertPublic(ModifierExamples.class);
    }

    @Test
    public void testAssertPublicMember2() {
        ReflectAssert.assertPublic("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"));
    }

    @Test
    public void testAssertPublicClass2() {
        ReflectAssert.assertPublic("a", ModifierExamples.class);
    }

    @Test
    public void testAssertPublicMemberFail1() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertPublic(ReflectHelper.getDeclaredField(ModifierExamples.class, "protectedField"))
        );
    }

    @Test
    public void testAssertPublicClassFail1() throws ClassNotFoundException {
        assertThrows(AssertionError.class, 
            () -> ReflectAssert.assertPublic(Class.forName("testutil.ModifierExamples$FinalClass"))
        );
    }

    @Test
    public void testAssertPublicMemberFail2() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertPublic("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "protectedField"))
        );
    }

    @Test
    public void testAssertPublicClassFail2() throws ClassNotFoundException {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertPublic("a", Class.forName("testutil.ModifierExamples$FinalClass"))
        );
    }

    @Test
    public void testAssertFinalMember1() {
        ReflectAssert.assertFinal(ReflectHelper.getDeclaredField(ModifierExamples.class, "finalField"));
    }

    @Test
    public void testAssertFinalClass1() throws ClassNotFoundException {
        ReflectAssert.assertFinal(Class.forName("testutil.ModifierExamples$FinalClass"));
    }

    @Test
    public void testAssertFinalMember2() {
        ReflectAssert.assertFinal("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "finalField"));
    }

    @Test
    public void testAssertFinalClass2() throws ClassNotFoundException {
        ReflectAssert.assertFinal("a", Class.forName("testutil.ModifierExamples$FinalClass"));
    }

    @Test
    public void testAssertFinalMemberFail1() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertFinal(ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertFinalClassFail1() throws ClassNotFoundException {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertFinal(Class.forName("testutil.ModifierExamples$ProtectedClass"))
        );
    }

    @Test
    public void testAssertFinalMemberFail2() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertFinal("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertFinalClassFail2() throws ClassNotFoundException {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertFinal("a", Class.forName("testutil.ModifierExamples$ProtectedClass"))
        );
    }

    @Test
    public void testAssertStaticMember1() {
        ReflectAssert.assertStatic(ReflectHelper.getDeclaredField(ModifierExamples.class, "staticField"));
    }

    @Test
    public void testAssertStaticClass1() throws ClassNotFoundException {
        ReflectAssert.assertStatic(Class.forName("testutil.ModifierExamples$StaticClass"));
    }

    @Test
    public void testAssertStaticMember2() {
        ReflectAssert.assertStatic("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "staticField"));
    }

    @Test
    public void testAssertStaticClass2() throws ClassNotFoundException {
        ReflectAssert.assertStatic("a", Class.forName("testutil.ModifierExamples$StaticClass"));
    }

    @Test
    public void testAssertStaticMemberFail1() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertStatic(ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertStaticClassFail1() throws ClassNotFoundException {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertStatic(Class.forName("testutil.ModifierExamples$ProtectedClass"))
        );
    }

    @Test
    public void testAssertStaticMemberFail2() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertStatic("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertStaticClassFail2() throws ClassNotFoundException{
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertStatic("a", Class.forName("testutil.ModifierExamples$ProtectedClass"))
        );
    }

    @Test
    public void testAssertNotStaticClassPass() throws ClassNotFoundException {
        ReflectAssert.assertNotStatic(Class.forName("testutil.ExampleArbitraryClass"));
    }

    @Test
    public void testAssertNotStaticClassFail() {
        assertThrows(AssertionError.class, () -> ReflectAssert.assertNotStatic(Class.forName("testutil.ModifierExamples$StaticClass")));
    }

    @Test
    public void testAssertNotStaticFieldPass() {
        ReflectAssert.assertNotStatic(ReflectHelper.getDeclaredField("testutil.ExampleArbitraryClass", "settable"));
    }

    @Test
    public void testAssertNotStaticFieldFail() {
        assertThrows(AssertionError.class, () ->
            ReflectAssert.assertNotStatic(ReflectHelper.getDeclaredField("testutil.ExampleArbitraryClass", "b"))
        );
    }

    @Test
    public void testAssertNotStaticMethodPass() {
        ReflectAssert.assertNotStatic(ReflectHelper.getDeclaredMethod("testutil.ExampleArbitraryClass", "m"));
    }

    @Test
    public void testAssertNotStaticMethodFail() {
        assertThrows(AssertionError.class, () -> ReflectHelper.getDeclaredField("testutil.ExampleArbitraryClass", "print"));
    }

    //

    @Test
    public void testAssertAbstractMember1() {
        ReflectAssert.assertAbstract(ReflectHelper.getDeclaredMethod(ModifierExamples.class, "abstractMethod"));
    }

    @Test
    public void testAssertAbstractClass1() {
        ReflectAssert.assertAbstract(ModifierExamples.class);
    }

    @Test
    public void testAssertAbstractMember2() {
        ReflectAssert.assertAbstract("a", ReflectHelper.getDeclaredMethod(ModifierExamples.class, "abstractMethod"));
    }

    @Test
    public void testAssertAbstractClass2() {
        ReflectAssert.assertAbstract("a", ModifierExamples.class);
    }

    @Test
    public void testAssertAbstractMemberFail1() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertAbstract(ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertAbstractClassFail1() throws ClassNotFoundException {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertAbstract(Class.forName("testutil.ModifierExamples$FinalClass"))
        );
    }

    @Test
    public void testAssertAbstractMemberFail2() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertAbstract("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertAbstractClassFail2() throws ClassNotFoundException {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertAbstract("a", Class.forName("testutil.ModifierExamples$FinalClass"))
        );
    }
    
    //

    @Test
    public void testAssertInterfaceClass1() throws ClassNotFoundException {
        ReflectAssert.assertInterface(Class.forName("testutil.ModifierExamples$InterfaceClass"));
    }

    @Test
    public void testAssertInterfaceClass2() throws ClassNotFoundException {
        ReflectAssert.assertInterface("a", Class.forName("testutil.ModifierExamples$InterfaceClass"));
    }

    @Test
    public void testAssertInterfaceMemberFail1() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertInterface(ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertInterfaceClassFail1() throws ClassNotFoundException {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertInterface(Class.forName("testutil.ModifierExamples$ProtectedClass"))
        );
    }

    @Test
    public void testAssertInterfaceMemberFail2() {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertInterface("a", ReflectHelper.getDeclaredField(ModifierExamples.class, "publicField"))
        );
    }

    @Test
    public void testAssertInterfaceClassFail2() throws ClassNotFoundException {
        assertThrows(AssertionError.class,
            () -> ReflectAssert.assertInterface("a", Class.forName("testutil.ModifierExamples$ProtectedClass"))
        );
    }

}
