package grading1331.gradescope.suite;

public class ClassWithBannedUsages {
    public void allSortsOfBadStuff() {
        var x = 3;
        var        y =    5;
        int[] a = {2, 3};
        int[] b = {3, 4};
        System.arraycopy(a, 0, b, 0, 2);
        System.exit(0);
    }
}
