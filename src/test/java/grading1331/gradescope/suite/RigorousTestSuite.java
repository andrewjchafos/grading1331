package grading1331.gradescope.suite;

import grading1331.ReflectHelper;
import grading1331.gradescope.GradedTest;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RigorousTestSuite {

    private static Class<?> doesNotExist;

    @BeforeEach
    public void setupClasses() {
        doesNotExist = ReflectHelper.getClass("DoesNotExist");
    }

    @GradedTest(name = "Only passes when Class is found", max_score = 5.0)
    public void alwaysPasses() {
        assertEquals(5, 5);
    }

    @GradedTest(name = "Should always fail", max_score = 4.0)
    public void alwaysFails() {
        assertEquals(1, doesNotExist.getDeclaredMethods().length);
    }
}
