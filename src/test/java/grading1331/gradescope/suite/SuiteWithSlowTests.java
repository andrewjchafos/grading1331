package grading1331.gradescope.suite;

import grading1331.ReflectHelper;
import grading1331.gradescope.GradedTest;
import grading1331.ReflectHelper.MethodInvocationReport;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class SuiteWithSlowTests {
    @GradedTest(name = "Really slow test")
    public void slowTest() throws InterruptedException {
        Thread.sleep(10000);
    }
    @GradedTest(name = "Infinite Loop")
    public void infiniteLoop() {
        while (true) {}
    }

    @GradedTest(name = "Infinite Loop in Student Code")
    public void infiniteLoopStudentCode() {
        Class<?> testClass = MyClass.class;
        MethodInvocationReport mir = ReflectHelper.invokeMethod(testClass, "infiniteMethod", null);
        assertFalse(mir.returnValue.isPresent(), "This output should not appear because the test is meant to time out");
        String output = mir.sysoutCapture.toString();
        assertFalse(output.isBlank(), "This output should not appear because the test is meant to time out");
    }
}

class MyClass {
    public static void infiniteMethod() {
        long a = 3;
        while (a > 0) {
            a++;
        }
    }
}