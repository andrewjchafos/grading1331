package grading1331.gradescope.suite;

import java.util.Scanner;
import java.util.HashMap;
import java.io.IOException;

public class ClassWithIllegalImports {
    public void doAThing() {
        throw new java.util.EmptyStackException();
    }
}
