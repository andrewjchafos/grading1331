package grading1331.gradescope;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class JavaFXApp extends Application{
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setScene(new Scene(new Group(), 300, 300, Color.ALICEBLUE));
        primaryStage.show();
    }
}
