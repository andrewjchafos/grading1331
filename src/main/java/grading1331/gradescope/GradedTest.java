package grading1331.gradescope;

import org.junit.jupiter.api.Test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An Annotation to be used in front of a method that is a member
 * of an Autograder Test class.  This annotation sets up several
 * of the properties needed in a Gradescope Test Result JSON String
 * <br>
 * Since this Annotation is already denoted as a JUnit 5 Test, the @Test
 * annotation is unnecessary when creating Autograder Test classes.
 * Heavily inspired by the existing Gradescope Annotation found
 * <a href="https://github.com/ucsb-gradescope-tools/sample-java-junit-ant-autograder/blob/master/src/com/gradescope/jh61b/grader/GradedTest.java">here</a>
 * @author Andrew Chafos
 */
@Test
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface GradedTest{
    /**
     * The Display Name of a given Test
     * @return The Display Name of a given Test
     */
    String name();
    // TODO figured out how this actually affects a Test
    String number() default "";

    /**
     * The score a student will receive on this test, if it passes.
     * @return The score a student will receive on this test, if it passes.
     */
    double max_score() default 1.0;

    /**
     * The visibility level of a Test, either Hidden or Visible
     * @return The visibility level of a Test, either Hidden or Visible
     */
    Visibility visibility() default Visibility.VISIBLE;
}
