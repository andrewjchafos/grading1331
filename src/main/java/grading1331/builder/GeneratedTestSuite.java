package grading1331.builder;

import java.util.List;

/**
 * An abstract class that a particular class should extend if any capabilities related
 * to generated tests are to be included in the list of tests executed by {@link grading1331.gradescope.GradedTestRunner}
 * @author Andrew Chafos
 */
public abstract class GeneratedTestSuite {
    /**
     * Returns a list of RunnableTest objects that will be converted into actual modular Runnable tests
     * that {@link grading1331.gradescope.GradedTestRunner} will run accordingly and then produce JSON output
     * based on their supplied metadata stored in an initial TestResult wrapper object
     * <br>
     * It is strongly recommended and preferred that the implementation of this method is simply returning the result
     * of one of the factory methods contained within one of the classes inside of this package.
     * @return a List of RunnableTest objects as described above
     */
    public abstract List<RunnableTest> getGeneratedTests();
}
