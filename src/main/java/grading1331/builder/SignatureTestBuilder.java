package grading1331.builder;

import grading1331.ReflectAssert;
import grading1331.ReflectHelper;
import grading1331.gradescope.Visibility;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * A class containing several factory methods used to generate a List of RunnableTest objects for
 * autograding the signatures of Class fields and methods.
 * <br>
 * The intended usage of the methods in this class is follows:
 * <ol>
 *   <li>An anonymous inner class that is an instance of {@link SignatureWrapper} is created with the precise signature
 *   of all the variables and methods to be searched for in the student class</li>
 *   <li>The name of the student class to test, as well as this anonymous inner class, are
 *   supplied to {@link #createBuilder(String, SignatureWrapper)}</li>
 *   <li>One or more of the methods related to test weight distribution, such as {@link #allSeparateTests(double)},
 *   is invoked on the temporary builder object returned from (2), returning a List of RunnableTest objects</li>
 *   <li>This list of RunnableTest objects is returned as the result of {@link GeneratedTestSuite#getGeneratedTests()}
 *   in some test class that extends {@link GeneratedTestSuite}</li>
 * </ol>
 * @author Andrew Chafos
 */
public class SignatureTestBuilder {

    private String studentClass;
    private SignatureWrapper wrapper;

    private SignatureTestBuilder() {}

    private SignatureTestBuilder(String studentClass, SignatureWrapper wrapper) {
        this.studentClass = studentClass;
        this.wrapper = wrapper;
    }

    public abstract static class SignatureWrapper {

    }

    /**
     * Creates a new SignatureTestBuilder object based on the supplied student class name and an anonymous inner class
     * containing signatures to test
     * @param studentClass the name of the class to test
     * @param wrapper an anonymous inner class containing the signatures of fields and methods to test
     * @return a builder object with fields matching the supplied parameters
     */
    public static SignatureTestBuilder createBuilder(String studentClass, SignatureWrapper wrapper) {
        return new SignatureTestBuilder(studentClass, wrapper);
    }

    /**
     * Returns a List of RunnableTest objects in which each RunnableTest corresponds to a single field or method
     * whose signature is being tested, and each is given the passed-in test weight with VISIBLE visibility,
     * and the String appearing before each test name is "StudentClass - "
     * @param testWeight the weight to assign every single signature tests
     * @return a list of RunnableTest objects for every single field or method to be tested
     */
    public List<RunnableTest> allSeparateTests(double testWeight) {
        return allSeparateTests(testWeight, Visibility.VISIBLE);
    }

    /**
     * Returns a List of RunnableTest objects in which each RunnableTest corresponds to a single field or method
     * whose signature is being tested, and each is given the passed-in test weight and visibility,
     * and the String appearing before each test name is "StudentClass - "
     * @param testWeight the weight to assign every single signature tests
     * @param visibility the visibility to assign every single signature tests
     * @return a list of RunnableTest objects for every single field or method to be tested
     */
    public List<RunnableTest> allSeparateTests(double testWeight, Visibility visibility) {
        return allSeparateTests(studentClass + " - ", testWeight, visibility);
    }

    /**
     * Returns a List of RunnableTest objects in which each RunnableTest corresponds to a single field or method
     * whose signature is being tested, and each is given the passed-in test weight and visibility, with the supplied
     * prefix appearing before every test name
     * @param testWeight the weight to assign every single signature tests
     * @param visibility the visibility to assign every single signature tests
     * @return a list of RunnableTest objects for every single field or method to be tested
     */
    public List<RunnableTest> allSeparateTests(String prefix, double testWeight, Visibility visibility) {
        List<Member> members = Stream.concat(Arrays.stream(wrapper.getClass().getDeclaredFields()), Arrays.stream(wrapper.getClass().getDeclaredMethods())).collect(Collectors.toList());
        members = members.stream().filter(m -> !m.getName().equals("this$0")).collect(Collectors.toList());
        return members.stream().map(m -> getMemberRunnableTest(m, prefix, testWeight, visibility)).collect(Collectors.toList());
    }

    private RunnableTest getMemberRunnableTest(Member member, String prefix, double testWeight, Visibility visibility) {
        Runnable toRun = () -> {
            Member studentMember = (member instanceof Field) ? ReflectHelper.getDeclaredField(studentClass, member.getName()) :
                    ReflectHelper.getDeclaredMethod(studentClass, member.getName(), ((Method) member).getParameterTypes());
            if (Modifier.isPrivate(member.getModifiers())) {
                ReflectAssert.assertPrivate(studentMember);
            } else if (Modifier.isProtected(member.getModifiers())) {
                ReflectAssert.assertProtected(studentMember);
            } else if (Modifier.isPublic(member.getModifiers())) {
                ReflectAssert.assertPublic(studentMember);
            } else {
                fail("The autograder does not currently support not adding visibility modifiers for this helper method.");
            }
            if (Modifier.isStatic(member.getModifiers())) {
                ReflectAssert.assertStatic(studentMember);
            } else {
                ReflectAssert.assertNotStatic(studentMember);
            }

            if (Modifier.isAbstract(member.getModifiers())) {
                ReflectAssert.assertAbstract(studentMember);
            } else {
                ReflectAssert.assertNotAbstract(studentMember);
            }

            if (Modifier.isFinal(member.getModifiers())) {
                ReflectAssert.assertFinal(studentMember);
            } else {
                ReflectAssert.assertNotFinal(studentMember);
            }

            if (member instanceof Method) {
                Method expectedMethod = (Method) member;
                String message = String.format("Your class should have a method named %s with return type %s", expectedMethod.getName(), expectedMethod.getReturnType().getSimpleName());
                assertEquals(expectedMethod.getReturnType(), ((Method) studentMember).getReturnType(), message);
            } else {
                Field expectedField = (Field) member;
                String message = String.format("Your class should have a field named %s of type %s", expectedField.getName(), expectedField.getType().getSimpleName());
                assertEquals(expectedField.getType(), ((Field) studentMember).getType(), message);
            }
        };
        String name = member.getName();
        if (member instanceof Method) {
            name += "(" + Arrays.stream(((Method) member).getParameterTypes()).map(Class::getSimpleName).collect(Collectors.joining(", ")) + ")";
        }
        return new RunnableTest(
            prefix + "check " + name + " has correct signature", "", testWeight, visibility, toRun
        );
    }

}
