package grading1331;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class JavaParserHelper {

    public static Map<MethodDeclaration, List<AnnotationExpr>> walkMethodAnnotations(CompilationUnit cu) {

        final Map<MethodDeclaration, List<AnnotationExpr>> annotations = new HashMap<>();

        VoidVisitorAdapter<Object> annotationVisitor = new VoidVisitorAdapter<>() {
            public void visit(MethodDeclaration method, Object arg) {
                if (method.getAnnotations() != null) {
                    for (AnnotationExpr annotation : method.getAnnotations()) {
                        List<AnnotationExpr> list = annotations.getOrDefault(method, new ArrayList<>());
                        list.add(annotation);
                        annotations.put(method, list);
                    }
                }
            }
        };

        annotationVisitor.visit(cu, null);

        return annotations;
    }


    public static void assertAnnotation(CompilationUnit cu,
            String annotationName, String methodName, String... paramTypes) {

        final Map<MethodDeclaration, List<AnnotationExpr>> annotations = walkMethodAnnotations(cu);

        for (MethodDeclaration method : annotations.keySet()) {
            if (method.getName().asString().equals(methodName) && paramTypes.length == method.getParameters().size()) {
                boolean correctTypes = true;
                for (int i = 0; i < paramTypes.length && correctTypes; i++) {
                    String typeString = method.getParameters().get(i).getType().asString();
                    if (!typeString.equals(paramTypes[i].toString())) {
                        correctTypes = false;
                    }
                }

                if (correctTypes) {
                    boolean foundAnnotation = false;
                    for (int i = 0; i < annotations.get(method).size() && !foundAnnotation; i++) {
                        AnnotationExpr annotation = annotations.get(method).get(i);
                        if (annotation.getName().asString().equals(annotationName)) {
                            foundAnnotation = true;
                        }
                    }

                    assertTrue(
                        foundAnnotation,
                        methodName + " doesn't have a " + annotationName + " annotation."
                    );
                    return;
                }
            }
        }

        fail("Either " + methodName + " doesn't have an " + annotationName + " annotation. Or "
            + methodName + " doesn't exist. Or its parameter types are wrong.");

    }


}
